#requires -version 2
<#
.SYNOPSIS
  <Short Description>
.DESCRIPTION
  <Long Descripttion of Script>
.PARAMETER <Parameter_Name>
    <Brief description of parameter input required. Repeat this attribute if required>
.INPUTS
  <Inputs if any, otherwise state None>
.OUTPUTS
  <Outputs if any, otherwise state None - example: Log file stored in C:\Windows\Temp\<name>.log>
.NOTES
  Version:        1.0
  Author:         <Name>
  Creation Date:  <Date>
  Purpose/Change: Initial script development
  
.EXAMPLE
  <Example goes here. Repeat this attribute for more than one example>
#>

#------------------------------------------------------------[Parameters]----------------------------------------------------------
#region Params
  # Define default parameter Set name 
  # [CmdletBinding(DefaultParameterSetName = "Template")]
  param(
    #[parameter(Mandatory = $true,
    #   ParameterSetName = "Template",  
    #   HelpMessage = "foo")]
    #[string]$foo,
  )

#endregion

#---------------------------------------------------------[Initialisations]--------------------------------------------------------
#region Init

#Set Error Action to Silently Continue
$ErrorActionPreference = "SilentlyContinue"

# Enable Strict PS Mode
Set-StrictMode -Version 2.0

#Dot Source required Function Libraries
. ".\Utils.ps1"

#endregion

#----------------------------------------------------------[Declarations]----------------------------------------------------------
#region Decl

#Script Version
$ScriptVersion = "1.0"

#Log File Info
$LogPath = "C:\Windows\Temp"
$LogName = "<script_name>.log"
$LogFile = Join-Path -Path $sLogPath -ChildPath $sLogName
#endregion

#-----------------------------------------------------------[Functions]------------------------------------------------------------
#region Func

<#
Function <FunctionName>{
  Param()
  
  Begin{
    Log-Write -LogPath $LogFile -LineValue "<description of what is going on>..."
  }
  
  Process{
    Try{
      <code goes here>
    }
    
    Catch{
      Log-Error -LogPath $LogFile -ErrorDesc $_.Exception -ExitGracefully $True
      Break
    }
  }
  
  End{
    If($?){
      Log-Write -LogPath $LogFile -LineValue "Completed Successfully."
      Log-Write -LogPath $LogFile -LineValue " "
    }
  }
}
#>

#endregion


#---------------------------------------------------------[Prerequisites]----------------------------------------------------------
#region Pre
# Used to install Modules etc.

<#
if (!$IsAdmin) {
  Write-Color -Text "Please restart the script in an elevated Powershell (Run as Administrator)" -Color Red
  Exit-Cleanup 2
}
if (Load-Module VMware.PowerCLI) {
  Write-Color -Text "PowerCLI successfully loaded" -Color White
}
else {
  Write-Color -Text "Installing PowerCLI" -Color White
  Install-PowerCLI
}
#>

#enregion
#-----------------------------------------------------------[Execution]------------------------------------------------------------
#region Main
#Log-Start -LogPath $sLogPath -LogName $sLogName -ScriptVersion $sScriptVersion
#Script Execution goes here
#Log-Finish -LogPath $LogFile
#endregion
